import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Main {

	public static void main(String[] args) {
		try {
			int i = 0;
			while (true) {
				i++;
				System.out.println("beep #" + i);
				play(args[0]);
				Thread.sleep(1000 * Long.parseLong(args[1]));
			}
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("It is not a supported Audio file");
		} catch (LineUnavailableException e) {
			System.err.println("Audio System is not Available.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("Not a valid file");
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("Second Argument must be a number in seconds.");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e) {
			// e.printStackTrace();
			System.err.println("Please Provide two arguments. <file name> <number of seconds to pause>");
		}
	}

	static void play(String path) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		AudioInputStream audioInputStream;
		String filePath = path;
		audioInputStream = AudioSystem.getAudioInputStream(new File(filePath).getAbsoluteFile());
		Clip clip;
		clip = AudioSystem.getClip();
		clip.open(audioInputStream);
		clip.start();
	}
}
